-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mydb
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

create database mydb;
use mydb;


--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `idUser` int(11) NOT NULL,
  `userName` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','21232F297A57A5A743894A0E4A801FC3');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `tecaj`
--

DROP TABLE IF EXISTS `tecaj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tecaj` (
  `idTecaj` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idTecaj`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `poglavje`
--

DROP TABLE IF EXISTS `poglavje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `poglavje` (
  `idPoglavje` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(45) DEFAULT NULL,
  `idTecaj` int(11) NOT NULL,
  `idNadrejenoPoglavje` int(11) DEFAULT NULL,
  `vsebina` longblob,
  PRIMARY KEY (`idPoglavje`),
  KEY `fk_Poglavje_Tecaj_idx` (`idTecaj`),
  CONSTRAINT `fk_Poglavje_Tecaj` FOREIGN KEY (`idTecaj`) REFERENCES `tecaj` (`idTecaj`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `zaporedje`
--

DROP TABLE IF EXISTS `zaporedje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zaporedje` (
  `idZaporedje` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(45) NOT NULL,
  `IdPrviClenZaporedja` int(11) DEFAULT NULL,
  PRIMARY KEY (`idZaporedje`)
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `clen_zaporedja`
--

DROP TABLE IF EXISTS `clen_zaporedja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clen_zaporedja` (
  `idClenZaporedja` int(11) NOT NULL AUTO_INCREMENT,
  `idZaporedje` int(11) NOT NULL,
  `ime` varchar(45) DEFAULT NULL,
  `vsebina` longblob,
  `idPrejsnjiClen` int(11) DEFAULT NULL,
  `idNaslednjiClen` int(11) DEFAULT NULL,
  PRIMARY KEY (`idClenZaporedja`),
  KEY `idZaporedje_idx` (`idZaporedje`) /*!80000 INVISIBLE */,
  KEY `idClenZaporedja_idx` (`idClenZaporedja`) /*!80000 INVISIBLE */,
  KEY `idNaslednjiClen_idx` (`idNaslednjiClen`),
  KEY `idPrejsnjiClen_idx` (`idPrejsnjiClen`),
  CONSTRAINT `idNaslednjiClen` FOREIGN KEY (`idNaslednjiClen`) REFERENCES `clen_zaporedja` (`idClenZaporedja`) ON DELETE CASCADE,
  CONSTRAINT `idPrejsnjiClen` FOREIGN KEY (`idPrejsnjiClen`) REFERENCES `clen_zaporedja` (`idClenZaporedja`) ON DELETE CASCADE,
  CONSTRAINT `idZaporedje` FOREIGN KEY (`idZaporedje`) REFERENCES `zaporedje` (`idZaporedje`)
) ENGINE=InnoDB AUTO_INCREMENT=298 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


ALTER TABLE `zaporedje` ADD CONSTRAINT fk_idPrviClenZaporedja FOREIGN KEY (IdPrviClenZaporedja) REFERENCES `clen_zaporedja` (`idClenZaporedja`);


/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-14 22:55:44
