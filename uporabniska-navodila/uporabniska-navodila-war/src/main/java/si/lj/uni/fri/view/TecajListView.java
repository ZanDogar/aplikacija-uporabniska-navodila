package si.lj.uni.fri.view;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import si.lj.uni.fri.constants.MessageConstants;
import si.lj.uni.fri.constants.WebConstants;
import si.lj.uni.fri.controller.PoglavjeController;
import si.lj.uni.fri.controller.TecajController;
import si.lj.uni.fri.exception.NavodilaException;
import si.lj.uni.fri.util.NavodilaUtil;
import si.lj.uni.fri.view.model.PoglavjeItem;
import si.lj.uni.fri.view.model.TecajItem;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Log4j
@Named
@ViewScoped
public class TecajListView extends FrameView implements Serializable {
    private static final long serialVersionUID = 5954663870483915803L;

    @Getter
    private List<TecajItem> tecajItemList;

    @Getter
    @Setter
    private TecajItem selectedTecaj;

    @Inject
    private TecajController tecajController;

    @Inject
    private PoglavjeController poglavjeController;

    @PostConstruct
    public void init(){
        if(loginView.getUser() == null){
            redirector.redirectToError(MessageConstants.ERROR_403);
            return;
        }
        try {
            tecajItemList = tecajController.getAll();
        } catch (NavodilaException e) {
            log.error(e);
        }
        selectedTecaj = new TecajItem();
    }

    public void dodajTecajClick(){
        selectedTecaj = new TecajItem();
        PrimeFaces.current().ajax().update(WebConstants.TECAJ_DIALOG);
        contextUtil.showDialog(WebConstants.TECAJ_DIALOG);
    }

    public void shraniTecaj(){
        try {
            if(selectedTecaj.getIdTecaj() != null) {
                tecajController.urediTecaj(selectedTecaj);
                messageUtil.showInfoMessage(MessageConstants.ZAPIS_SHRANJEN, new Object[]{selectedTecaj.getIme()});

            } else{
                tecajController.dodajTecaj(selectedTecaj);
                messageUtil.showInfoMessage(MessageConstants.TECAJ_DODAN, new Object[]{selectedTecaj.getIme()});
            }
        } catch (NavodilaException e) {
            log.error(e);
        }
        refresh();
    }

    public void izbrisiTecaj(){
        if(selectedTecaj != null && selectedTecaj.getIdTecaj() != null) {
            try {
                tecajController.izbrisiTecaj(selectedTecaj.getIdTecaj());
                messageUtil.showInfoMessage(MessageConstants.TECAJ_IZBRISAN, new Object[]{selectedTecaj.getIme()});
            } catch (NavodilaException e) {
                log.error(e);
                messageUtil.showErrorMessage(MessageConstants.NAPAKA_BAZA);
                return;
            }
        }
        refresh();
    }

    public String povezava(Long idTecaj){
        return String.format(redirector.REDIRECT_TO_TECAJ_URL, idTecaj);
    }

    public StreamedContent prenesiTecaj(Long idTecaj) {
        ByteArrayInputStream bis = new ByteArrayInputStream(getFiles(idTecaj));
        InputStream stream = bis;

        StringBuilder sbImeDatoteke = new StringBuilder();
        sbImeDatoteke.append("tecaj-id-");
        sbImeDatoteke.append(idTecaj);
        sbImeDatoteke.append(".zip");
        return new DefaultStreamedContent(stream, "application/zip", sbImeDatoteke.toString(),  StandardCharsets.UTF_8.name());
    }

    private byte[] getFiles(Long idTecaj){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(baos);

        try{
            List<PoglavjeItem> poglavjeItemList =poglavjeController.getPoglavjaTecaja(idTecaj);
            if(!NavodilaUtil.isListNullOrEmpty(poglavjeItemList)) {
                ZipEntry entry;
                for(PoglavjeItem poglavjeItem : poglavjeItemList){
                    entry = new ZipEntry(poglavjeItem.getIme() + ".html");
                    zos.putNextEntry(entry);
                    zos.write(poglavjeItem.getVsebina() != null ? poglavjeItem.getVsebina().getBytes() : "".getBytes());
                }
            }
        }catch(NavodilaException | IOException e){
            log.error(e);
            messageUtil.showErrorMessage(MessageConstants.NAPAKA_PRENOS);
        } finally {
            try {
                zos.close();
                baos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return baos.toByteArray();
    }

    private void refresh(){
        redirector.redirectToTecaji();
    }
}