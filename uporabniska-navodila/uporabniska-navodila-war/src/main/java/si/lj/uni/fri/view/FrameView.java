package si.lj.uni.fri.view;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import si.lj.uni.fri.constants.MessageConstants;
import si.lj.uni.fri.controller.TecajController;
import si.lj.uni.fri.enums.Parameter;
import si.lj.uni.fri.exception.NavodilaException;
import si.lj.uni.fri.redirect.Redirector;
import si.lj.uni.fri.util.ContextUtil;
import si.lj.uni.fri.util.MessageUtil;
import si.lj.uni.fri.util.NavodilaUtil;
import si.lj.uni.fri.view.model.TecajItem;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Log4j
@Named
@ViewScoped
public class FrameView implements Serializable {

    private static final long serialVersionUID = -2504583766666428984L;

    @Inject
    private TecajController tecajController;

    @Inject
    protected ContextUtil contextUtil;

    @Inject
    protected MessageUtil messageUtil;

    @Inject
    protected Redirector redirector;

    @Inject
    protected LoginView loginView;

    @Getter
    private MenuModel model;

    @Getter
    @Setter
    private String imeTecaja;

    private List<TecajItem> tecajItemList;

    @Getter
    private boolean topBarRendered = false;

    @PostConstruct
    public void init() {
        log.info("frame init");
        fillCourseModel();

        String paramTopBarRendered = contextUtil.getRequestParameter(Parameter.TOPBAR.ime());
        topBarRendered = loginView.getUser() != null || Boolean.valueOf(paramTopBarRendered);
    }

    protected void fillCourseModel(){
        model = new DefaultMenuModel();
        try {
            tecajItemList = tecajController.getAll();
        } catch (NavodilaException e) {
            log.error(e);
        }
        //First submenu
        DefaultSubMenu firstSubmenu = new DefaultSubMenu(messageUtil.getResourceMessage(MessageConstants.LABEL_TECAJ));

        DefaultMenuItem item;
        if(!NavodilaUtil.isListNullOrEmpty(tecajItemList)) {
            for (TecajItem tecajItem : tecajItemList) {
                item = new DefaultMenuItem(tecajItem.getIme());
                item.setId(String.valueOf(tecajItem.getIdTecaj()));
                item.setCommand("#{frameView.redirectToTecaj(" + tecajItem.getIdTecaj() + ")}");
                firstSubmenu.addElement(item);
            }
        }
        model.addElement(firstSubmenu);
    }

    /**
     * Metoda se klice ob kliku na posamezen tecaj v meniju tecajev.
     *
     * @param idTecaj je idTecaja
     */
    public void redirectToTecaj(String idTecaj) {
        redirector.redirectToTecaj(idTecaj);
    }

}
