package si.lj.uni.fri.view;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import org.primefaces.PrimeFaces;
import si.lj.uni.fri.constants.MessageConstants;
import si.lj.uni.fri.constants.WebConstants;
import si.lj.uni.fri.controller.UserController;
import si.lj.uni.fri.exception.NavodilaException;
import si.lj.uni.fri.util.ContextUtil;
import si.lj.uni.fri.util.MessageUtil;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Log4j
@Named
@ViewScoped
public class ChangePasswordView implements Serializable {
    private static final long serialVersionUID = -6314223082802472263L;

    @Inject
    private UserController userController;

    @Inject
    private LoginView loginView;

    @Inject
    private MessageUtil messageUtil;

    @Inject
    private ContextUtil contextUtil;

    @Getter
    @Setter
    private String password;

    public void spremeniGeslo() {
        try {
            userController.spremeniGeslo(loginView.getUser().getIdUser(), password);
        } catch (NavodilaException e) {
            log.error(e);
            messageUtil.showErrorMessage(MessageConstants.NAPAKA_BAZA);
            return;
        }

        contextUtil.closeDialog(WebConstants.CHANGE_PASSWORD_DIALOG);
        messageUtil.showInfoMessage(MessageConstants.USPESNA_MENJAVA_GESLA);
        PrimeFaces.current().ajax().update(WebConstants.CONTENT_FORM);
    }
}
