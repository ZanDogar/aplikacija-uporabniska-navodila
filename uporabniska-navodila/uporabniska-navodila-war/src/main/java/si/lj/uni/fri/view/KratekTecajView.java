package si.lj.uni.fri.view;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import org.primefaces.PrimeFaces;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import si.lj.uni.fri.constants.MessageConstants;
import si.lj.uni.fri.constants.WebConstants;
import si.lj.uni.fri.controller.ClenZaporedjaController;
import si.lj.uni.fri.controller.ZaporedjeController;
import si.lj.uni.fri.enums.Parameter;
import si.lj.uni.fri.exception.NavodilaException;
import si.lj.uni.fri.view.model.ClenZaporedjaItem;
import si.lj.uni.fri.view.model.ZaporedjeItem;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

import static si.lj.uni.fri.redirect.Redirector.REDIRECT_TO_KRATEK_TECAJ_POGLAVJE_URL;

@Log4j
@Named
@ViewScoped
public class KratekTecajView extends FrameView implements Serializable {
    private static final long serialVersionUID = -3244121727756171236L;

    @Inject
    private ZaporedjeController zaporedjeController;

    @Inject
    private ClenZaporedjaController clenZaporedjaController;

    private Long idKratekTecaj;
    private Long idClenZaporedja;

    @Getter
    private boolean meniRendered = true;

    @Getter
    private MenuModel model;

    @Getter
    @Setter
    private ZaporedjeItem zaporedjeItem;

    @Getter
    @Setter
    private ClenZaporedjaItem selectedClenZaporedjaItem = new ClenZaporedjaItem();

    @Getter
    @Setter
    private String imePoglavja;

    @Override
    @PostConstruct
    public void init(){
        String paramIdKratekTecaj = contextUtil.getRequestParameter(Parameter.ID.ime());
        String paramIdClenZaporedja = contextUtil.getRequestParameter(Parameter.ID_CLEN_ZAPOREDJA.ime());
        String paramMeni = contextUtil.getRequestParameter(Parameter.MENI.ime());

        if(paramIdKratekTecaj != null){
            idKratekTecaj = Long.parseLong(paramIdKratekTecaj);
            getKratekTecaj();

            if(paramIdClenZaporedja != null){
                idClenZaporedja = Long.parseLong(paramIdClenZaporedja);
            }
            getClenZaporedja();

            if(paramMeni != null){
                meniRendered = Boolean.parseBoolean(paramMeni);
            }
            fillModel();
        }

    }

    private void getKratekTecaj(){
        try {
            zaporedjeItem = zaporedjeController.getById(idKratekTecaj);
        } catch (NavodilaException e) {
            log.error(e);
            messageUtil.showErrorMessage(e.getMessage());
        }
    }

    private void getClenZaporedja(){
        if(zaporedjeItem != null && idClenZaporedja != null){
            selectedClenZaporedjaItem = zaporedjeItem.getClenZaporedjaItemList().stream()
                    .filter(clenZaporedjaItem -> idClenZaporedja.equals(clenZaporedjaItem.getIdClenZaporedja()))
                    .findAny()
                    .orElse(null);

            if(selectedClenZaporedjaItem == null){
                redirector.redirectToError(MessageConstants.ERROR_404);
                return;
            }
        }
        else if(zaporedjeItem != null && zaporedjeItem.getIdPrviClenZaporedja() != null){
            selectedClenZaporedjaItem = zaporedjeItem.getIdPrviClenZaporedja();

            if(selectedClenZaporedjaItem == null) {
                selectedClenZaporedjaItem = new ClenZaporedjaItem();
            }
        }
    }

    private void fillModel(){
        if(meniRendered) {
            model = new DefaultMenuModel();
            DefaultSubMenu defaultSubMenu = new DefaultSubMenu();
            model.addElement(defaultSubMenu);
            if (zaporedjeItem.getIdPrviClenZaporedja() != null) {
                dodajNaslednjiClenMenija(defaultSubMenu, zaporedjeItem.getIdPrviClenZaporedja());
            }
        }
    }

    private void dodajNaslednjiClenMenija(DefaultSubMenu defaultSubMenu, ClenZaporedjaItem clenZaporedjaItem){
        DefaultMenuItem defaultMenuItem = new DefaultMenuItem(clenZaporedjaItem.getIme(), "pi pi-angle-right", getMeniItemRedirectUrl(clenZaporedjaItem));
        defaultSubMenu.addElement(defaultMenuItem);
        if(clenZaporedjaItem.getIdNaslednjiClen() != null){
            dodajNaslednjiClenMenija(defaultSubMenu, clenZaporedjaItem.getIdNaslednjiClen());
        }
    }

    private String getMeniItemRedirectUrl(ClenZaporedjaItem clenZaporedjaItem){
        return String.format(REDIRECT_TO_KRATEK_TECAJ_POGLAVJE_URL, idKratekTecaj, clenZaporedjaItem.getIdClenZaporedja());
    }

    public void saveVsebina(){
        try {
            selectedClenZaporedjaItem.setVsebina(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("editordata"));
            selectedClenZaporedjaItem = clenZaporedjaController.save(selectedClenZaporedjaItem, zaporedjeItem);
        } catch (NavodilaException e) {
            log.error(e);
             messageUtil.showErrorMessage(e.getMessage());
        }
        messageUtil.showInfoMessage(MessageConstants.ZAPIS_SHRANJEN);
        refresh();
    }

    public void clearVsebina(){
        selectedClenZaporedjaItem.setVsebina(null);
        try {
            selectedClenZaporedjaItem = clenZaporedjaController.save(selectedClenZaporedjaItem, zaporedjeItem);
        } catch (NavodilaException e) {
            log.error(e);
        }
        messageUtil.showInfoMessage(MessageConstants.ZAPIS_IZBRISAN);
        PrimeFaces.current().ajax().update(WebConstants.CONTENT_FORM);
    }

    public void prejsnjePoglavje(){
        changePoglavje(selectedClenZaporedjaItem.getLongPrejsnjiClen());
    }

    public void naslednjePoglavje(){
        if(selectedClenZaporedjaItem.getLongNaslednjiClen() != null) {
            changePoglavje(selectedClenZaporedjaItem.getLongNaslednjiClen());
        } else{
            messageUtil.showErrorMessage(MessageConstants.NASLEDNJE_POGLAVJE_NE_OBSTAJA);
        }
    }

    public void changePoglavje(Long idClenZaporedja){
        redirector.redirectToKratekTecajPoglavje(idKratekTecaj, idClenZaporedja);
    }

    public void dodajNaslednjiClen(){
        try {
            clenZaporedjaController.dodajNaslednjiClen(imePoglavja, selectedClenZaporedjaItem, zaporedjeItem);
        } catch (NavodilaException e) {
            log.error(e);
            messageUtil.showErrorMessage(MessageConstants.NAPAKA_BAZA);
        }
        refresh();
    }

    public void izbrisiNaslednjiClen(){
        try {
            clenZaporedjaController.izbrisiNaslednjiClen(selectedClenZaporedjaItem.getIdClenZaporedja());
        } catch (NavodilaException e) {
            log.error(e);
            messageUtil.showErrorMessage(MessageConstants.NAPAKA_BAZA);
        }
        refresh();
    }

    private void refresh(){
        getKratekTecaj();
        getClenZaporedja();
        fillModel();
        PrimeFaces.current().ajax().update(WebConstants.CONTENT_FORM);
    }
}