package si.lj.uni.fri.view;

import lombok.Getter;
import si.lj.uni.fri.constants.MessageConstants;
import si.lj.uni.fri.enums.Parameter;
import si.lj.uni.fri.util.ContextUtil;
import si.lj.uni.fri.util.MessageUtil;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class ErrorView implements Serializable {

    @Getter
    private String errorMessage;

    @Inject
    private MessageUtil messageUtil;

    @Inject
    private ContextUtil contextUtil;

    @PostConstruct
    public void init(){
        String s = contextUtil.getRequestParameter(Parameter.ERROR_MESSAGE.ime());
        errorMessage = messageUtil.getResourceMessage(s != null ? s : MessageConstants.ERROR_404);
    }
}