package si.lj.uni.fri.view;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import si.lj.uni.fri.constants.MessageConstants;
import si.lj.uni.fri.constants.WebConstants;
import si.lj.uni.fri.controller.PoglavjeController;
import si.lj.uni.fri.controller.TecajController;
import si.lj.uni.fri.enums.Parameter;
import si.lj.uni.fri.exception.NavodilaException;
import si.lj.uni.fri.util.NavodilaUtil;
import si.lj.uni.fri.view.model.NavodilaMeniItem;
import si.lj.uni.fri.view.model.PoglavjeItem;
import si.lj.uni.fri.view.model.TecajItem;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Log4j
@Named
@ViewScoped
public class TecajView extends FrameView implements Serializable {

    private static final long serialVersionUID = -8319613147714209788L;

    @Inject
    private TecajController tecajController;

    @Inject
    private PoglavjeController poglavjeController;

    @Getter
    @Setter
    private TecajItem tecajItem;

    @Getter
    private TreeNode root;

    @Getter
    private boolean meniRendered = true;

    @Getter
    @Setter
    private TreeNode selectedNode;

    @Getter
    @Setter
    private PoglavjeItem selectedPoglavje = new PoglavjeItem();

    @Getter
    @Setter
    private String imePoglavja;

    private Long idTecaj;
    private Long idPoglavje;

    @Override
    @PostConstruct
    public void init(){
        String paramIdTecaj = contextUtil.getRequestParameter(Parameter.ID.ime());
        String paramIdPoglavje = contextUtil.getRequestParameter(Parameter.ID_POGLAVJE.ime());
        String paramMeni = contextUtil.getRequestParameter(Parameter.MENI.ime());
        if(paramIdTecaj != null) {
            idTecaj = Long.parseLong(paramIdTecaj);
            getTecaj(idTecaj);
            if(paramMeni != null){
                meniRendered = Boolean.parseBoolean(paramMeni);
            }
            napolniLeviMeni();
        }
        if(paramIdPoglavje != null){
            idPoglavje = Long.parseLong(paramIdPoglavje);
            nastaviSelectedNode(root, idPoglavje);
            onTreeNodeSelect(false);
        }

    }

    private void getTecaj(Long idTecaj){
        try {
            tecajItem = tecajController.getById(idTecaj);
        } catch (NavodilaException e) {
            log.error(e);
        }
    }

    private void napolniLeviMeni(){
        root = new DefaultTreeNode(new NavodilaMeniItem(tecajItem.getIdTecaj(), tecajItem.getIme()), null);
        for (PoglavjeItem poglavjeItem : tecajItem.getPoglavjeItemList()) {
            TreeNode treeNode = new DefaultTreeNode(new NavodilaMeniItem(poglavjeItem), root);
            treeNode.setExpanded(true);
            if (!NavodilaUtil.isListNullOrEmpty(poglavjeItem.getPodrejenaPoglavjaList())) {
                dodajNivo(poglavjeItem.getPodrejenaPoglavjaList(), treeNode);
            }
        }
    }

    private void dodajNivo(List<PoglavjeItem> poglavjeItemList, TreeNode treeNode){
        for(PoglavjeItem poglavjeItem : poglavjeItemList){
            TreeNode newTreeNode = new DefaultTreeNode(new NavodilaMeniItem(poglavjeItem), treeNode);
            newTreeNode.setExpanded(true);
            if(!NavodilaUtil.isListNullOrEmpty(poglavjeItem.getPodrejenaPoglavjaList())){
                dodajNivo(poglavjeItem.getPodrejenaPoglavjaList(), newTreeNode);
            }
        }
    }

    public void onTreeNodeSelect(boolean redirect) {
        try {
            NavodilaMeniItem navodilaMeniItem = (NavodilaMeniItem) selectedNode.getData();
            selectedPoglavje = poglavjeController.getById(navodilaMeniItem.getId());
            selectedPoglavje.setIdPrejsnjePoglavje(navodilaMeniItem.getIdPrejsnjePoglavje());
            selectedPoglavje.setIdNaslednjePoglavje(navodilaMeniItem.getIdNaslednjePoglavje());
            if(redirect) {
                redirector.redirectToTecajPoglavje(String.valueOf(idTecaj), String.valueOf(selectedPoglavje.getIdPoglavje()), meniRendered);
            }
        } catch (NavodilaException e) {
            log.error(e);
        }
        PrimeFaces.current().ajax().update(WebConstants.CONTENT_FORM);
    }

    public void saveVsebina(){
        try {
            selectedPoglavje.setVsebina(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("editordata"));
            selectedPoglavje = poglavjeController.save(selectedPoglavje, tecajItem);
        } catch (NavodilaException e) {
            log.error(e);
            messageUtil.showErrorMessage(MessageConstants.NAPAKA_BAZA);
        }
        messageUtil.showInfoMessage(MessageConstants.ZAPIS_SHRANJEN);
        PrimeFaces.current().ajax().update(WebConstants.CONTENT_FORM);
        reloadPoglavja();
        nastaviSelectedNode(root, idPoglavje);
    }

    public void clearVsebina(){
        selectedPoglavje.setVsebina(null);
        try {
            selectedPoglavje = poglavjeController.save(selectedPoglavje);
        } catch (NavodilaException e) {
            log.error(e);
        }
        messageUtil.showInfoMessage(MessageConstants.ZAPIS_IZBRISAN);
        PrimeFaces.current().ajax().update(WebConstants.CONTENT_FORM);
    }

    public void dodajPoglavje(){
        try {
            poglavjeController.dodajPoglavje(imePoglavja, idTecaj, selectedNode != null ? ((NavodilaMeniItem) selectedNode.getData()).getId() : null);
        } catch (NavodilaException e) {
            log.error(e);
            messageUtil.showErrorMessage(e.getMessage());
        }
        getTecaj(idTecaj);
        napolniLeviMeni();
        PrimeFaces.current().ajax().update(WebConstants.CONTENT_FORM);
    }

    public void izbrisiPoglavje(){
        if(selectedNode != null && selectedNode.getData() != null){
            NavodilaMeniItem poglavje = (NavodilaMeniItem) selectedNode.getData();
            if(poglavje.getId() != null){
                try {
                    poglavjeController.izbrisiPoglavje(poglavje.getId(), poglavje.getPodrejenaPoglavjaList());
                    reloadPoglavja();
                } catch (NavodilaException e) {
                    log.error(e);
                    messageUtil.showErrorMessage(e.getMessage());
                }
            }
        }
    }

    public void prejsnjePoglavje(){
        changePoglavje(selectedPoglavje.getIdPrejsnjePoglavje());
    }

    public void naslednjePoglavje(){
        changePoglavje(selectedPoglavje.getIdNaslednjePoglavje());
    }

    public void changePoglavje(Long idPoglavje){
        try {
            selectedPoglavje = poglavjeController.getById(idPoglavje);
            nastaviSelectedNode(root, idPoglavje);
            NavodilaMeniItem navodilaMeniItem = (NavodilaMeniItem) selectedNode.getData();
            selectedPoglavje.setIdPrejsnjePoglavje(navodilaMeniItem.getIdPrejsnjePoglavje());
            selectedPoglavje.setIdNaslednjePoglavje(navodilaMeniItem.getIdNaslednjePoglavje());
            redirector.redirectToTecajPoglavje(String.valueOf(idTecaj), String.valueOf(idPoglavje), meniRendered);
        } catch (NavodilaException e) {
            log.error(e);
            messageUtil.showErrorMessage(e.getMessage());
        }
    }

    private void nastaviSelectedNode(TreeNode treeNode, Long idPoglavje){
       if(treeNode != null && !NavodilaUtil.isListNullOrEmpty(treeNode.getChildren())){
           treeNode.setSelected(false);
           for(TreeNode tmpTreeNode : treeNode.getChildren()){
               NavodilaMeniItem navodilaMeniItem = (NavodilaMeniItem) tmpTreeNode.getData();
               if(navodilaMeniItem != null && navodilaMeniItem.getId().equals(idPoglavje)){
                   tmpTreeNode.setSelected(true);
                   selectedNode = tmpTreeNode;
               } else{
                   tmpTreeNode.setSelected(false);
                   nastaviSelectedNode(tmpTreeNode, idPoglavje);
               }
           }
       }
    }

    private void reloadPoglavja(){
        getTecaj(idTecaj);
        napolniLeviMeni();
        PrimeFaces.current().ajax().update(WebConstants.CONTENT_FORM);
    }
}