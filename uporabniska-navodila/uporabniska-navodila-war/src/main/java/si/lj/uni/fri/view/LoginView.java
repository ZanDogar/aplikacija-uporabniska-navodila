package si.lj.uni.fri.view;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import org.primefaces.PrimeFaces;
import si.lj.uni.fri.constants.MessageConstants;
import si.lj.uni.fri.constants.WebConstants;
import si.lj.uni.fri.controller.UserController;
import si.lj.uni.fri.database.entity.User;
import si.lj.uni.fri.exception.NavodilaException;
import si.lj.uni.fri.util.ContextUtil;
import si.lj.uni.fri.util.MessageUtil;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Log4j
@Named
@SessionScoped
public class LoginView implements Serializable {
    private static final long serialVersionUID = -6314223082802472263L;

    @Inject
    private UserController userController;

    @Inject
    private MessageUtil messageUtil;
    @Inject
    private ContextUtil contextUtil;

    @Getter
    @Setter
    private String userName;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private User user;

    public void login() {
        try {
            user = userController.getByUserLogin(userName, password);
        } catch (NavodilaException e) {
            log.error(e);
            messageUtil.showErrorMessage(e.getMessage());
            return;
        }
        if(user != null){
            messageUtil.showInfoMessage(MessageConstants.USPESNA_PRIJAVA);
            contextUtil.closeDialog(WebConstants.LOGIN_DIALOG);
        }
        PrimeFaces.current().ajax().update(WebConstants.CONTENT_FORM);
        PrimeFaces.current().ajax().update(WebConstants.TOPBAR_FORM);
    }

    public void odjava(){
        user = null;
        messageUtil.showInfoMessage(MessageConstants.USPESNA_ODJAVA);
        PrimeFaces.current().ajax().update(WebConstants.CONTENT_FORM);
        PrimeFaces.current().ajax().update(WebConstants.TOPBAR_FORM);

    }
}
