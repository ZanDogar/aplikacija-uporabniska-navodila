package si.lj.uni.fri.view;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.StreamedContent;
import si.lj.uni.fri.constants.MessageConstants;
import si.lj.uni.fri.constants.WebConstants;
import si.lj.uni.fri.controller.ClenZaporedjaController;
import si.lj.uni.fri.controller.PoglavjeController;
import si.lj.uni.fri.controller.TecajController;
import si.lj.uni.fri.controller.ZaporedjeController;
import si.lj.uni.fri.database.entity.ClenZaporedja;
import si.lj.uni.fri.exception.NavodilaException;
import si.lj.uni.fri.util.NavodilaUtil;
import si.lj.uni.fri.view.model.ClenZaporedjaItem;
import si.lj.uni.fri.view.model.PoglavjeItem;
import si.lj.uni.fri.view.model.TecajItem;
import si.lj.uni.fri.view.model.ZaporedjeItem;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Log4j
@Named
@ViewScoped
public class KratekTecajListView extends FrameView implements Serializable {
    private static final long serialVersionUID = 7965848829814331998L;

    @Getter
    private List<ZaporedjeItem> zaporedjeItemList;

    @Getter
    @Setter
    private ZaporedjeItem selectedZaporedje;

    @Getter
    @Setter
    private DualListModel<PoglavjeItem> poglavjeItemPickList = new DualListModel<>();

    @Getter
    @Setter
    private TecajItem selectedTecaj;

    @Getter
    @Setter
    private List<TecajItem> tecajItemList;

    @Getter
    @Setter
    private List<PoglavjeItem> poglavjaTecaja;

    @Getter
    @Setter
    private boolean zacniIzTecaja;

    @Inject
    private ZaporedjeController zaporedjeController;

    @Inject
    private TecajController tecajController;

    @Inject
    private PoglavjeController poglavjeController;

    @Inject
    private ClenZaporedjaController clenZaporedjaController;

    @Override
    @PostConstruct
    public void init() {
        if (loginView.getUser() == null) {
            redirector.redirectToError(MessageConstants.ERROR_403);
            return;
        }
        try{
            zaporedjeItemList = zaporedjeController.getAll();
        } catch (NavodilaException e){
            log.error(e);
        }
        selectedZaporedje = new ZaporedjeItem();
    }

    public void dodajZaporedjeClick(){
        selectedZaporedje = new ZaporedjeItem();
        zacniIzTecaja = false;
        fillTecajItemList();
        PrimeFaces.current().ajax().update(WebConstants.KRATEK_TECAJ_DIALOG);
        contextUtil.showDialog(WebConstants.KRATEK_TECAJ_DIALOG);
    }

    public void shraniZaporedje(){
        try {
            if(selectedZaporedje.getIdZaporedje() != null) {
                zaporedjeController.urediZaporedje(selectedZaporedje);
                messageUtil.showInfoMessage(MessageConstants.ZAPIS_SHRANJEN, new Object[]{selectedZaporedje.getIme()});

            } else{
                List<ClenZaporedja> cziList = null;
                if(zacniIzTecaja && !NavodilaUtil.isListNullOrEmpty(poglavjeItemPickList.getTarget())){
                    cziList = poglavjeItemPickList.getTarget().stream()
                            .map(poglavjeItem -> clenZaporedjaController.poglavjeItemToClenZaporedja(poglavjeItem))
                            .collect(Collectors.toList());

                }
                zaporedjeController.dodajZaporedje(selectedZaporedje, cziList);
                messageUtil.showInfoMessage(MessageConstants.KRATEK_TECAJ_DODAN, new Object[]{selectedZaporedje.getIme()});
            }
        } catch (NavodilaException e) {
            log.error(e);
            messageUtil.showErrorMessage(MessageConstants.NAPAKA_BAZA);
        }
        contextUtil.closeDialog(WebConstants.KRATEK_TECAJ_DIALOG);
        refresh();
    }

    public void izbrisiZaporedje(){
        if(selectedZaporedje != null && selectedZaporedje.getIdZaporedje() != null) {
            try {
                zaporedjeController.izbrisiZaporedje(selectedZaporedje.getIdZaporedje());
                messageUtil.showInfoMessage(MessageConstants.KRATEK_TECAJ_IZBRISAN, new Object[]{selectedZaporedje.getIme()});
            } catch (NavodilaException e) {
                log.error(e);
            }
        }
        refresh();
    }

    private void refresh(){
        try{
            zaporedjeItemList = zaporedjeController.getAll();
            PrimeFaces.current().ajax().update(WebConstants.CONTENT_FORM);
        } catch (NavodilaException e){
            log.error(e);
        }
    }

    private void fillTecajItemList(){
        try {
            tecajItemList = tecajController.getAll();
            if(!NavodilaUtil.isListNullOrEmpty(tecajItemList)){
                selectedTecaj = tecajItemList.get(0);
                fillPoglavjeItemPickList();
            }
        } catch (NavodilaException e) {
            log.error(e);
        }
    }


    public void fillPoglavjeItemPickList(){
        try {
            if(selectedTecaj != null && selectedTecaj.getIdTecaj() != null) {
                List<PoglavjeItem> pickListSource = poglavjeController.getPoglavjaTecaja(selectedTecaj.getIdTecaj());
                List<PoglavjeItem> pickListTarget = new ArrayList<>();
                poglavjeItemPickList = new DualListModel<>(pickListSource, pickListTarget);
            }
        } catch (NavodilaException e) {
            log.error(e);
        }
    }

    public String povezava(Long idZaporedje){
        return String.format(redirector.REDIRECT_TO_KRATEK_TECAJ_URL, idZaporedje);
    }

    public StreamedContent prenesiTecaj(Long idZaporedje) {
        ByteArrayInputStream bis = new ByteArrayInputStream(getFiles(idZaporedje));
        InputStream stream = bis;

        StringBuilder sbImeDatoteke = new StringBuilder();
        sbImeDatoteke.append("kratekTecaj-id-");
        sbImeDatoteke.append(idZaporedje);
        sbImeDatoteke.append(".zip");
        return new DefaultStreamedContent(stream, "application/zip", sbImeDatoteke.toString(),  StandardCharsets.UTF_8.name());
    }

    private byte[] getFiles(Long idZaporedje){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(baos);

        try{
            ZaporedjeItem zaporedjeItem = zaporedjeController.getById(idZaporedje);
            if(zaporedjeItem.getIdPrviClenZaporedja() != null) {
                ClenZaporedjaItem tmpClen = zaporedjeItem.getIdPrviClenZaporedja();
                while (tmpClen != null){
                    ZipEntry entry = new ZipEntry(tmpClen.getIme() + ".html");
                    zos.putNextEntry(entry);
                    zos.write(tmpClen.getVsebina() != null ? tmpClen.getVsebina().getBytes() : "".getBytes());
                    tmpClen = tmpClen.getIdNaslednjiClen();
                }
            }
        }catch(NavodilaException | IOException e){
            log.error(e);
            messageUtil.showErrorMessage(MessageConstants.NAPAKA_PRENOS);
        } finally {
            try {
                zos.close();
                baos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return baos.toByteArray();
    }
}
