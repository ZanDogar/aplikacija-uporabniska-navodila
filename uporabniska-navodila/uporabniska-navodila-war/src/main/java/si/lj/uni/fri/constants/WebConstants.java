package si.lj.uni.fri.constants;

public class WebConstants {

    public static final String CONTENT_FORM = "contentForm";
    public static final String TOPBAR_FORM = "topbarForm";
    public static final String LOGIN_DIALOG = "loginDialog";
    public static final String TECAJ_DIALOG = "tecajDialog";
    public static final String KRATEK_TECAJ_DIALOG = "kratekTecajDialog";
    public static final String CHANGE_PASSWORD_DIALOG = "changePasswordDialog";
}