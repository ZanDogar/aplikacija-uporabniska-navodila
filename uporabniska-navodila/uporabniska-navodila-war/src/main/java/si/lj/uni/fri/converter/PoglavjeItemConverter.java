package si.lj.uni.fri.converter;

import lombok.extern.log4j.Log4j;
import si.lj.uni.fri.controller.PoglavjeController;
import si.lj.uni.fri.exception.NavodilaException;
import si.lj.uni.fri.view.model.PoglavjeItem;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Log4j
@FacesConverter("poglavjeItemConverter")
public class PoglavjeItemConverter implements Converter {
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        PoglavjeController poglavjeController = CDI.current().select(PoglavjeController.class).get();
        try {
            return poglavjeController.getById(Long.valueOf(s));
        } catch (NavodilaException e) {
            log.error(e);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        return String.valueOf(((PoglavjeItem)o).getIdPoglavje());
    }
}
