package si.lj.uni.fri.controller;

import si.lj.uni.fri.dao.PoglavjeDao;
import si.lj.uni.fri.database.entity.Poglavje;
import si.lj.uni.fri.exception.NavodilaException;
import si.lj.uni.fri.mappers.PoglavjeMapper;
import si.lj.uni.fri.util.NavodilaUtil;
import si.lj.uni.fri.view.model.PoglavjeItem;
import si.lj.uni.fri.view.model.TecajItem;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
public class PoglavjeController implements Serializable {

    @Inject
    private PoglavjeDao poglavjeDao;

    @Inject
    private TecajController tecajController;

    @Inject
    private PoglavjeMapper poglavjeMapper;



    public PoglavjeItem getById(Long idPoglavje) throws NavodilaException {
        return poglavjeMapper.toPoglavjeItem(poglavjeDao.getById(idPoglavje));
    }

    public List<PoglavjeItem> getPoglavjaTecaja(Long idTecaj) throws NavodilaException {
        return poglavjeMapper.toPoglavjeItemList(poglavjeDao.getPoglavjaTecaja(idTecaj));
    }

    public PoglavjeItem save(PoglavjeItem poglavjeItem) throws NavodilaException {
        poglavjeDao.save(poglavjeMapper.toPoglavje(poglavjeItem));
        return getById(poglavjeItem.getIdPoglavje());
    }

    public PoglavjeItem save(PoglavjeItem poglavjeItem, TecajItem tecajItem) throws NavodilaException {
        tecajController.saveIme(tecajItem);
        return save(poglavjeItem);
    }

    public void dodajPoglavje(String ime, Long idTecaj, Long idNadrejenoPoglavje) throws NavodilaException {
        Poglavje poglavje = new Poglavje();
        poglavje.setIme(ime);
        poglavje.setIdTecaj(idTecaj);
        poglavje.setIdNadrejenoPoglavje(idNadrejenoPoglavje);
        poglavjeDao.dodajPoglavje(poglavje);
    }

    public void izbrisiPoglavje(Long idPoglavje, List<PoglavjeItem> podrejenaPoglavjaList) throws NavodilaException {
        List<Long> vsaPoglavja = new ArrayList<>();
        vsaPoglavja.add(idPoglavje);
        getVsaPodrejenaPoglavja(podrejenaPoglavjaList, vsaPoglavja);
        poglavjeDao.izbrisiPoglavja(vsaPoglavja);
    }

    private void getVsaPodrejenaPoglavja(List<PoglavjeItem> podrejenaPoglavjaList, List<Long> result){
        if(!NavodilaUtil.isListNullOrEmpty(podrejenaPoglavjaList)){
            for(PoglavjeItem poglavjeItem : podrejenaPoglavjaList){
                result.add(poglavjeItem.getIdPoglavje());
                getVsaPodrejenaPoglavja(poglavjeItem.getPodrejenaPoglavjaList(), result);
            }
        }
    }
}
