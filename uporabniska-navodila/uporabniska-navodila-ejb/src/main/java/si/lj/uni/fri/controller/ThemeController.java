package si.lj.uni.fri.controller;

import lombok.Getter;
import lombok.Setter;
import si.lj.uni.fri.view.model.Theme;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Named
@ApplicationScoped
public class ThemeController {

    @Getter
    private List<Theme> themes;

    @Getter
    @Setter
    private Theme selectedTheme;

    @PostConstruct
    public void init() {
        FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale("sl_SI"));

        themes = new ArrayList<>();
        themes.add(new Theme(0, "Nova-Light", "nova-light"));
        themes.add(new Theme(1, "Nova-Dark", "nova-dark"));
        themes.add(new Theme(2, "Nova-Colored", "nova-colored"));
        themes.add(new Theme(3, "Luna-Blue", "luna-blue"));
        themes.add(new Theme(4, "Luna-Amber", "luna-amber"));
        themes.add(new Theme(5, "Luna-Green", "luna-green"));
        themes.add(new Theme(6, "Luna-Pink", "luna-pink"));
        themes.add(new Theme(7, "Omega", "omega"));

        selectedTheme = themes.get(5);
    }

    public void saveTheme(){
        System.out.println("saveTheme()");
    }
}