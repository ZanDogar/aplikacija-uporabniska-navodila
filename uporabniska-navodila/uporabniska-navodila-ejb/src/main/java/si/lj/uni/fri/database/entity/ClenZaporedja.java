package si.lj.uni.fri.database.entity;


import lombok.Data;

import javax.persistence.*;

@Entity(name = "clen_zaporedja")
@Table(name = "clen_zaporedja")
@Data
public class ClenZaporedja {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idClenZaporedja;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idZaporedje")
    private Zaporedje zaporedje;

    @Column
    private String ime;

    @Column
    private byte[] vsebina;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="idPrejsnjiClen")
    private ClenZaporedja idPrejsnjiClen;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="idNaslednjiClen")
    private ClenZaporedja idNaslednjiClen;
}
