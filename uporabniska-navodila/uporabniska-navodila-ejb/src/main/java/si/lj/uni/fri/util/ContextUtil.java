package si.lj.uni.fri.util;

import org.primefaces.PrimeFaces;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class ContextUtil {

    @Inject
    private MessageUtil messageUtil;

    public String getRequestParameter(String parameterName){
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(NavodilaUtil.stringToHex(parameterName));
    }

    public void showDialog(String dialogName){
        StringBuilder sb = new StringBuilder();
        sb.append("PF('");
        sb.append(dialogName);
        sb.append("').show();");
        PrimeFaces.current().executeScript(sb.toString());
    }

    public void closeDialog(String dialogName){
        StringBuilder sb = new StringBuilder();
        sb.append("PF('");
        sb.append(dialogName);
        sb.append("').hide();");
        PrimeFaces.current().executeScript(sb.toString());
    }
}
