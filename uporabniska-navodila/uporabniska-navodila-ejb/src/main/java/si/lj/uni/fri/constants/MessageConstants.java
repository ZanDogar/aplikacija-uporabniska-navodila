package si.lj.uni.fri.constants;

public class MessageConstants {

    public static final String NAPAKA_BAZA = "napaka_baza";
    public static final String ZAPIS_SHRANJEN = "zapis_shranjen";
    public static final String ZAPIS_IZBRISAN = "zapis_izbrisan";
    public static final String USPESNA_PRIJAVA = "uspesna_prijava";
    public static final String USPESNA_MENJAVA_GESLA = "uspesna_menjava_gesla";
    public static final String USPESNA_ODJAVA = "uspesna_odjava";
    public static final String UPORABNIK_NE_OBSTAJA = "uporabnik_ne_obstaja";
    public static final String TECAJ_DODAN = "tecaj_dodan";
    public static final String TECAJ_IZBRISAN = "tecaj_izbrisan";
    public static final String LABEL_TECAJ = "label_tecaj";
    public static final String ERROR_403 = "error_403";
    public static final String ERROR_404 = "error_404";
    public static final String KRATEK_TECAJ_DODAN = "kratek_tecaj_dodan";
    public static final String KRATEK_TECAJ_IZBRISAN = "kratek_tecaj_izbrisan";
    public static final String NASLEDNJE_POGLAVJE_NE_OBSTAJA = "naslednje_poglavje_ne_obstaja";
    public static final String NAPAKA_PRENOS = "napaka_prenos";
}
