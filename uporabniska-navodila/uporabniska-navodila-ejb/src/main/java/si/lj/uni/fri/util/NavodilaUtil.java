package si.lj.uni.fri.util;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Named
@RequestScoped
public class NavodilaUtil {

    public static boolean isListNullOrEmpty(List list){
        return list == null || list.isEmpty();
    }

    public static String byteArrayToString(byte[] byteArray) {
        return byteArray != null ? new String(byteArray, StandardCharsets.UTF_8) : null;
    }

    public static byte[] stringToByteArray(String s){
        return s != null ? s.getBytes(StandardCharsets.UTF_8) : null ;
    }

    public static String stringToHex(String s) {
        StringBuilder sb = new StringBuilder();
        for (char ch : s.toCharArray()) {
            sb.append(Integer.toHexString(ch));
        }
        return sb.toString();
    }

    public static String hexToString(String s) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < s.length(); i += 2) {
            String str = s.substring(i, i + 2);
            sb.append((char) Integer.parseInt(str, 16));
        }
        return sb.toString();
    }
}
