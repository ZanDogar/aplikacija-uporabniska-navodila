package si.lj.uni.fri.exception;

public class NavodilaException extends Exception {

    public NavodilaException(Throwable cause){
        super(cause);
    }

    public NavodilaException(String message){
        super(message);
    }
}
