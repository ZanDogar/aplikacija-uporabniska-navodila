package si.lj.uni.fri.database.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity(name = "zaporedje")
@Table(name = "zaporedje")
@Data
public class Zaporedje {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idZaporedje;

    @Column
    private String ime;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "idZaporedje")
    private List<ClenZaporedja> clenZaporedjaList;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="idPrviClenZaporedja")
    private ClenZaporedja idPrviClenZaporedja;
}
