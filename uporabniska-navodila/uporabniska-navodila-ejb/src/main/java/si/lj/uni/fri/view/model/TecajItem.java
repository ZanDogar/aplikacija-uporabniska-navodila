package si.lj.uni.fri.view.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TecajItem implements Serializable {

    private Long idTecaj;
    private String ime;
    private List<PoglavjeItem> poglavjeItemList;

    public TecajItem(Long idTecaj, String ime){
        this.idTecaj = idTecaj;
        this.ime = ime;
    }

    @Override
    public String toString() {
        return String.valueOf(idTecaj);
    }
}
