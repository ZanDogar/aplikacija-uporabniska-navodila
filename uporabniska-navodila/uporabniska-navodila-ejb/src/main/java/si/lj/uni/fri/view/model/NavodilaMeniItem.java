package si.lj.uni.fri.view.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode
@AllArgsConstructor
public class NavodilaMeniItem implements Serializable, Comparable<NavodilaMeniItem> {

    private Long id;
    private String name;
    private List<PoglavjeItem> podrejenaPoglavjaList;
    private Long idPrejsnjePoglavje;
    private Long idNaslednjePoglavje;

    public NavodilaMeniItem(Long id, String name){
        this.id = id;
        this.name = name;
    }

    public NavodilaMeniItem(PoglavjeItem poglavjeItem){
        this.id = poglavjeItem.getIdPoglavje();
        this.name = poglavjeItem.getIme();
        this.podrejenaPoglavjaList = poglavjeItem.getPodrejenaPoglavjaList();
        this.idPrejsnjePoglavje = poglavjeItem.getIdPrejsnjePoglavje();
        this.idNaslednjePoglavje = poglavjeItem.getIdNaslednjePoglavje();
    }

    @Override
    public String toString() {
        return name;
    }

    public int compareTo(NavodilaMeniItem navodilaMeniItem) {
        return this.getName().compareTo(navodilaMeniItem.getName());
    }
}
