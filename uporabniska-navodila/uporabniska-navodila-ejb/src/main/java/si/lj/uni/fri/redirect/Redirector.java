package si.lj.uni.fri.redirect;

import lombok.extern.log4j.Log4j;
import si.lj.uni.fri.enums.Parameter;
import si.lj.uni.fri.util.NavodilaUtil;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;

@Log4j
@RequestScoped
@Named
public class Redirector {

    public static final String REDIRECT_TO_TECAJ_URL = "tecaj.xhtml?" + NavodilaUtil.stringToHex(Parameter.ID.ime()) + "=%s";
    private static final String REDIRECT_TO_TECAJ_LIST_URL = "tecajList.xhtml";
    private static final String REDIRECT_TO_ERROR_URL = "errors/error.xhtml?" + NavodilaUtil.stringToHex(Parameter.ERROR_MESSAGE.ime()) + "=";
    private static final String REDIRECT_TO_TECAJ_POGLAVJE_URL = "tecaj.xhtml?" + NavodilaUtil.stringToHex(Parameter.ID.ime()) + "=%s&" + NavodilaUtil.stringToHex(Parameter.ID_POGLAVJE.ime()) +"=%s&" + NavodilaUtil.stringToHex(Parameter.MENI.ime()) + "=%s";
    private static final String REDIRECT_TO_KRATEK_TECAJ_LIST_URL = "kratekTecajList.xhtml";
    public static final String REDIRECT_TO_KRATEK_TECAJ_URL = "kratekTecaj.xhtml?" + NavodilaUtil.stringToHex(Parameter.ID.ime()) + "=%d&";
    public static final String REDIRECT_TO_KRATEK_TECAJ_POGLAVJE_URL = "kratekTecaj.xhtml?" + NavodilaUtil.stringToHex(Parameter.ID.ime()) + "=%d&" + NavodilaUtil.stringToHex(Parameter.ID_CLEN_ZAPOREDJA.ime()) +"=%d";


    public void redirectToTecaj(String idTecaj){
        redirect(String.format(REDIRECT_TO_TECAJ_URL, idTecaj));
    }

    public void redirectToTecaji(){
        redirect(REDIRECT_TO_TECAJ_LIST_URL);
    }

    public void redirectToTecajPoglavje(String idTecaj, String idPoglavje, boolean meniRendered){
        redirect(String.format(REDIRECT_TO_TECAJ_POGLAVJE_URL, idTecaj, idPoglavje, meniRendered));
    }

    public void redirectToKratekTecajList(){
        redirect(REDIRECT_TO_KRATEK_TECAJ_LIST_URL);
    }

    public void redirectToKratekTecajPoglavje(Long idKratekTecaj, Long idPoglavje){
        redirect(String.format(REDIRECT_TO_KRATEK_TECAJ_POGLAVJE_URL, idKratekTecaj, idPoglavje));
    }

    public void redirectToError(String errorMessage) {
        redirect(REDIRECT_TO_ERROR_URL + errorMessage);
    }

    private void redirect(String url) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        try {
            ec.redirect(url);
        } catch (IOException e) {
            log.error("Malformed URL: " + url);
        }
    }
}
