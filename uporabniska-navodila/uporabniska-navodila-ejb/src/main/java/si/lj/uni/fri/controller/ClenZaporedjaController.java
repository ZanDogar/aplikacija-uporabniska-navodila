package si.lj.uni.fri.controller;

import si.lj.uni.fri.dao.ClenZaporedjaDao;
import si.lj.uni.fri.dao.ZaporedjeDao;
import si.lj.uni.fri.database.entity.ClenZaporedja;
import si.lj.uni.fri.database.entity.Zaporedje;
import si.lj.uni.fri.exception.NavodilaException;
import si.lj.uni.fri.mappers.ClenZaporedjaMapper;
import si.lj.uni.fri.mappers.ZaporedjeMapper;
import si.lj.uni.fri.view.model.ClenZaporedjaItem;
import si.lj.uni.fri.view.model.PoglavjeItem;
import si.lj.uni.fri.view.model.ZaporedjeItem;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
public class ClenZaporedjaController implements Serializable {

    @Inject
    private ClenZaporedjaDao clenZaporedjaDao;

    @Inject
    private ZaporedjeDao zaporedjeDao;

    @Inject
    private ClenZaporedjaMapper clenZaporedjaMapper;

    @Inject
    private ZaporedjeMapper zaporedjeMapper;

    public ClenZaporedja poglavjeItemToClenZaporedja(PoglavjeItem poglavjeItem){
        return clenZaporedjaMapper.poglavjeItemToClenZaporedja(poglavjeItem, false);
    }

    public ClenZaporedjaItem getById(Long idClenZaporedja) throws NavodilaException {
        return clenZaporedjaMapper.toClenZaporedjaItem(clenZaporedjaDao.getById(idClenZaporedja), true);
    }

    public ClenZaporedjaItem save(ClenZaporedjaItem clenZaporedjaItem, ZaporedjeItem zaporedjeItem) throws NavodilaException {
        Zaporedje zaporedje = zaporedjeMapper.toZaporedje(zaporedjeItem);
        ClenZaporedja clenZaporedja = clenZaporedjaMapper.toClenZaporedja(clenZaporedjaItem, zaporedje);
        zaporedjeDao.urediZaporedje(zaporedje.getIdZaporedje(), zaporedje.getIme());
        clenZaporedjaDao.save(clenZaporedja);
        return getById(clenZaporedjaItem.getIdClenZaporedja());
    }

    public void dodajNaslednjiClen(String ime, ClenZaporedjaItem clenZaporedjaItem, ZaporedjeItem zaporedjeItem) throws NavodilaException {
        Zaporedje zaporedje = zaporedjeMapper.toZaporedje(zaporedjeItem);

        ClenZaporedja naslednjiClen = new ClenZaporedja();
        naslednjiClen.setZaporedje(zaporedje);
        naslednjiClen.setIme(ime);

        ClenZaporedja clenZaporedja = clenZaporedjaMapper.toClenZaporedja(clenZaporedjaItem, zaporedje);

        naslednjiClen = clenZaporedjaDao.dodajClenZaporedja(naslednjiClen);
        naslednjiClen.setIdPrejsnjiClen(clenZaporedja);
        clenZaporedja.setIdNaslednjiClen(naslednjiClen);
        clenZaporedjaDao.save(naslednjiClen);
        clenZaporedjaDao.save(clenZaporedja);
    }

    public void izbrisiNaslednjiClen(Long idClenZaporedja) throws NavodilaException {
        clenZaporedjaDao.izbrisiNaslednjiClen(idClenZaporedja);
    }
}
