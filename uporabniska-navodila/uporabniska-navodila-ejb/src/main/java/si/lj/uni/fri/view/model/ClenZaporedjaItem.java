package si.lj.uni.fri.view.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClenZaporedjaItem implements Serializable {

    private Long idClenZaporedja;
    private ZaporedjeItem zaporedjeItem;
    private String ime;
    private String vsebina;
    private ClenZaporedjaItem idPrejsnjiClen;
    private ClenZaporedjaItem idNaslednjiClen;

    private Long longNaslednjiClen;
    private Long longPrejsnjiClen;

    @Override
    public String toString() {
        return "ClenZaporedjaItem{" +
                "idClenZaporedja=" + idClenZaporedja +
                '}';
    }
}
