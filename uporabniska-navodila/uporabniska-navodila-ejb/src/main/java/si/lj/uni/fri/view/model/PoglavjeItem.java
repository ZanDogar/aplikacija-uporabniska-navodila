package si.lj.uni.fri.view.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PoglavjeItem implements Serializable {
    private Long idPoglavje;
    private String ime;
    private Long idNadrejenoPoglavje;
    private List<PoglavjeItem> podrejenaPoglavjaList;
    private Long idTecaj;
    private String vsebina;
    private Long idPrejsnjePoglavje;
    private Long idNaslednjePoglavje;

    private PoglavjeItem prejsnjePoglavje;
    private PoglavjeItem naslednjePoglavje;

    @Override
    public String toString() {
        return String.valueOf(idPoglavje);
    }
}