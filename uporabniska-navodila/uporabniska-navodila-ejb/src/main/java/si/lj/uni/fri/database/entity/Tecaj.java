package si.lj.uni.fri.database.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@Entity(name = "tecaj")
@Table(name = "tecaj")
@Data
@NoArgsConstructor
@NamedQueries({
        @NamedQuery(
                name = "Tecaj.getById",
                query = "SELECT t FROM tecaj t LEFT JOIN poglavje p ON t.idTecaj = p.idTecaj WHERE t.idTecaj = :idTecaj"
        )
})
public class Tecaj {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idTecaj;

    @Column
    private String ime;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "idTecaj")
    private List<Poglavje> poglavjeList;

    public Tecaj(String ime){
        this.ime = ime;
    }
}
