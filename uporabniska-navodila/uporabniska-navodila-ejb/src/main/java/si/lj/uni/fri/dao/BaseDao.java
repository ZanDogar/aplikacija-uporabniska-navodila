package si.lj.uni.fri.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class BaseDao {

    @PersistenceContext(unitName = "UporabniskaNavodilaDB")
    protected EntityManager em;
}
