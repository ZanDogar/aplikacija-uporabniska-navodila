package si.lj.uni.fri.controller;

import si.lj.uni.fri.dao.TecajDao;
import si.lj.uni.fri.exception.NavodilaException;
import si.lj.uni.fri.mappers.TecajMapper;
import si.lj.uni.fri.util.NavodilaUtil;
import si.lj.uni.fri.view.model.PoglavjeItem;
import si.lj.uni.fri.view.model.TecajItem;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Named
public class TecajController implements Serializable {

    @Inject
    private TecajDao tecajDao;

    @Inject
    private TecajMapper tecajMapper;

    public List<TecajItem> getAll() throws NavodilaException {
        return tecajMapper.toTecajItemList(tecajDao.getAll());
    }

    public TecajItem getById(Long idTecaj) throws NavodilaException {
        TecajItem tecajItem = tecajMapper.toTecajItem(tecajDao.getById(idTecaj));
        tecajItem.setPoglavjeItemList(sestaviPoglavja(tecajItem.getPoglavjeItemList()));
        return tecajItem;
    }

    public void dodajTecaj(TecajItem tecajItem) throws NavodilaException {
        tecajDao.dodajTecaj(tecajMapper.toTecaj(tecajItem));
    }

    public void urediTecaj(TecajItem tecajItem) throws NavodilaException {
        tecajDao.urediTecaj(tecajMapper.toTecaj(tecajItem));
    }

    public void izbrisiTecaj(Long idTecaj) throws NavodilaException {
        tecajDao.izbrisiTecaj(idTecaj);
    }

    public void saveIme(TecajItem tecajItem) throws NavodilaException {
        tecajDao.saveIme(tecajItem.getIdTecaj(), tecajItem.getIme());
    }

    private List<PoglavjeItem> sestaviPoglavja(List<PoglavjeItem> poglavjeItemList){
        List<PoglavjeItem> result = new ArrayList<>();
        if(poglavjeItemList != null){
            for (PoglavjeItem poglavjeItem : poglavjeItemList) {
                poglavjeItem.setPodrejenaPoglavjaList(findPodrejenaPoglavja(poglavjeItemList, poglavjeItem.getIdPoglavje()));

                if(!NavodilaUtil.isListNullOrEmpty(poglavjeItem.getPodrejenaPoglavjaList())){
                    for (int i = 0; i < poglavjeItem.getPodrejenaPoglavjaList().size(); i++) {
                        if(i > 0) {
                            poglavjeItem.getPodrejenaPoglavjaList().get(i-1).setIdNaslednjePoglavje(poglavjeItem.getPodrejenaPoglavjaList().get(i).getIdPoglavje());
                        }
                        if(i < poglavjeItem.getPodrejenaPoglavjaList().size()-1){
                            poglavjeItem.getPodrejenaPoglavjaList().get(i+1).setIdPrejsnjePoglavje(poglavjeItem.getPodrejenaPoglavjaList().get(i).getIdPoglavje());
                        }
                    }
                }

                if (poglavjeItem.getIdNadrejenoPoglavje() == null) {
                    result.add(poglavjeItem);
                }
            }
        }
        return result;
    }

    private List<PoglavjeItem> findPodrejenaPoglavja(List<PoglavjeItem> poglavjeItemList, Long idPoglavje){
        return poglavjeItemList.stream()
                .filter(poglavjeItem -> idPoglavje.equals(poglavjeItem.getIdNadrejenoPoglavje()))
                .collect(Collectors.toList());
    }
}


