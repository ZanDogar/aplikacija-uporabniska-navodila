package si.lj.uni.fri.dao;

import lombok.extern.log4j.Log4j;
import si.lj.uni.fri.database.entity.Zaporedje;
import si.lj.uni.fri.exception.NavodilaException;
import si.lj.uni.fri.util.NavodilaUtil;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import java.util.List;

@Log4j
@Stateless
public class ZaporedjeDao extends BaseDao {

    @Inject
    private ClenZaporedjaDao clenZaporedjaDao;

    public List<Zaporedje> getAll() throws NavodilaException {
        List<Zaporedje> resultList;
        try{
            resultList = em.createQuery("select z from zaporedje z").getResultList();
        }catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
        return resultList;
    }

    public Zaporedje getById(Long idZaporedje) throws NavodilaException {
        Zaporedje zaporedje;
        try {
            zaporedje = (Zaporedje) em.createQuery("select z from zaporedje z where z.idZaporedje = :idZaporedje")
                    .setParameter("idZaporedje", idZaporedje)
                    .getSingleResult();
        } catch (NoResultException e){
            log.error(e);
            throw new NavodilaException(e);
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
        return zaporedje;
    }

    @Transactional
    public void dodajZaporedje(Zaporedje zaporedje) throws NavodilaException {
        try {

            em.persist(zaporedje);

            if(!NavodilaUtil.isListNullOrEmpty(zaporedje.getClenZaporedjaList())){
                zaporedje.setIdPrviClenZaporedja(zaporedje.getClenZaporedjaList().get(0));
            }
            em.persist(zaporedje);
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
    }

    public void urediZaporedje(Long idZaporedje, String ime) throws NavodilaException {
        try {
            em.createQuery("update zaporedje z set z.ime = :ime where z.idZaporedje = :idZaporedje")
                    .setParameter("ime", ime)
                    .setParameter("idZaporedje", idZaporedje)
                    .executeUpdate();
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
    }

    @Transactional
    public void izbrisiZaporedje(Long idZaporedje) throws NavodilaException {
        try {
            em.createQuery("update zaporedje z set z.idPrviClenZaporedja = :idPrviClenZaporedja where z.idZaporedje = :idZaporedje")
                    .setParameter("idPrviClenZaporedja", null)
                    .setParameter("idZaporedje", idZaporedje)
                    .executeUpdate();

            clenZaporedjaDao.izbrisiCleneZaporedja(idZaporedje);
            em.createQuery("delete from zaporedje z where z.idZaporedje = :idZaporedje")
                    .setParameter("idZaporedje", idZaporedje)
                    .executeUpdate();
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
    }
}
