package si.lj.uni.fri.dao;

import lombok.extern.log4j.Log4j;
import si.lj.uni.fri.constants.MessageConstants;
import si.lj.uni.fri.database.entity.User;
import si.lj.uni.fri.exception.NavodilaException;

import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.StringJoiner;

@Log4j
@Named
public class UserDao extends BaseDao {

    public User getByUserLogin(String userName, String password) throws NavodilaException {
        User result;

        try {
            StringJoiner sj = new StringJoiner(" ");
            sj.add("select u from user u");
            sj.add("where userName = :userName");
            sj.add("and password = :password");

            Query query = em.createQuery(sj.toString())
                    .setParameter("userName", userName)
                    .setParameter("password", password);

            result = (User) query.getSingleResult();
        } catch (NoResultException e) {
            log.error(e);
            throw new NavodilaException(MessageConstants.UPORABNIK_NE_OBSTAJA);
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }

        return result;
    }

    @Transactional
    public void spremeniGeslo(Long idUser, String password) throws NavodilaException {
        try{
            em.createQuery("update user u set u.password = :password where u.idUser = :idUser")
                    .setParameter("password", password)
                    .setParameter("idUser", idUser)
                    .executeUpdate();
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
    }
}