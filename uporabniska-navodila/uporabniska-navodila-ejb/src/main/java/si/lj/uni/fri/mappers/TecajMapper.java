package si.lj.uni.fri.mappers;

import si.lj.uni.fri.database.entity.Tecaj;
import si.lj.uni.fri.util.NavodilaUtil;
import si.lj.uni.fri.view.model.TecajItem;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.stream.Collectors;

@Named
public class TecajMapper {

    @Inject
    private PoglavjeMapper poglavjeMapper;

    public TecajItem toTecajItem(Tecaj tecaj) {
        TecajItem tecajItem = new TecajItem();
        if (tecaj != null) {
            tecajItem.setIdTecaj(tecaj.getIdTecaj());
            tecajItem.setIme(tecaj.getIme());

            if(tecaj.getPoglavjeList() != null){
                tecajItem.setPoglavjeItemList(poglavjeMapper.toPoglavjeItemList(tecaj.getPoglavjeList()));
            }
        }
        return tecajItem;
    }

    public List<TecajItem> toTecajItemList(List<Tecaj> tecajList){
        return tecajList.stream()
                .map(this::toTecajItem)
                .collect(Collectors.toList());
    }

    public Tecaj toTecaj(TecajItem tecajItem){
        Tecaj tecaj = new Tecaj();
        if(tecajItem != null){
            tecaj.setIdTecaj(tecajItem.getIdTecaj());
            tecaj.setIme(tecajItem.getIme());

            if(!NavodilaUtil.isListNullOrEmpty(tecajItem.getPoglavjeItemList())){
                tecaj.setPoglavjeList(poglavjeMapper.toPoglavjeList(tecajItem.getPoglavjeItemList()));
            }
        }
        return tecaj;
    }
}
