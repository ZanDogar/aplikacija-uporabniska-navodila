package si.lj.uni.fri.dao;

import lombok.extern.log4j.Log4j;
import si.lj.uni.fri.database.entity.Tecaj;
import si.lj.uni.fri.exception.NavodilaException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import java.util.List;

@Log4j
@Stateless
public class TecajDao extends BaseDao {

    @Inject
    private PoglavjeDao poglavjeDao;

    public List<Tecaj> getAll() throws NavodilaException {
        List<Tecaj> resultList;
        try {
            String query = "SELECT t.idTecaj, t.ime FROM tecaj t";
            resultList = em.createNativeQuery(query, Tecaj.class).getResultList();
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
        return resultList;
    }

    public Tecaj getById(Long idTecaj) throws NavodilaException {
        Tecaj tecaj = null;
        try {
            tecaj = (Tecaj) em.createNamedQuery("Tecaj.getById")
                    .setParameter("idTecaj", idTecaj)
                    .getSingleResult();
        } catch (NoResultException e){
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
        return tecaj;
    }

    public void dodajTecaj(Tecaj tecaj) throws NavodilaException {
        try {
            em.persist(tecaj);
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
    }


    public void urediTecaj(Tecaj tecaj) throws NavodilaException {
       try {
           em.merge(tecaj);
       } catch (PersistenceException e){
           log.error(e);
           throw new NavodilaException(e);
       }
    }

    @Transactional
    public void izbrisiTecaj(Long idTecaj) throws NavodilaException{
        try {
            poglavjeDao.izbrisiPoglavjaTecaja(idTecaj);
            em.createQuery("delete from tecaj t where t.idTecaj = :idTecaj")
                    .setParameter("idTecaj", idTecaj)
                    .executeUpdate();
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
    }

    public void saveIme(Long idTecaj, String ime) throws NavodilaException {
        try {
            em.createQuery("update tecaj t set t.ime = :ime where t.idTecaj = :idTecaj")
                    .setParameter("ime", ime)
                    .setParameter("idTecaj", idTecaj)
                    .executeUpdate();
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
    }
}
