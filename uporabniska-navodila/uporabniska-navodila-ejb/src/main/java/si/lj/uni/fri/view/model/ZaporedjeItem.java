package si.lj.uni.fri.view.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ZaporedjeItem implements Serializable {

    private Long idZaporedje;
    private String ime;
    private List<ClenZaporedjaItem> clenZaporedjaItemList;
    private ClenZaporedjaItem idPrviClenZaporedja;
}
