package si.lj.uni.fri.mappers;

import si.lj.uni.fri.database.entity.Poglavje;
import si.lj.uni.fri.util.NavodilaUtil;
import si.lj.uni.fri.view.model.PoglavjeItem;

import javax.inject.Named;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Named
public class PoglavjeMapper {

    public PoglavjeItem toPoglavjeItem(Poglavje poglavje){
        PoglavjeItem poglavjeItem = new PoglavjeItem();
        if(poglavje != null){
            poglavjeItem.setIdPoglavje(poglavje.getIdPoglavje());
            poglavjeItem.setIme(poglavje.getIme());
            poglavjeItem.setIdTecaj(poglavje.getIdTecaj());
            poglavjeItem.setIdNadrejenoPoglavje(poglavje.getIdNadrejenoPoglavje());
            poglavjeItem.setVsebina(NavodilaUtil.byteArrayToString(poglavje.getVsebina()));
        }
        return poglavjeItem;
    }

    public List<PoglavjeItem> toPoglavjeItemList(List<Poglavje> poglavjeList){
        return poglavjeList.stream()
                .map(this::toPoglavjeItem)
                .collect(Collectors.toList());
    }

    public Poglavje toPoglavje(PoglavjeItem poglavjeItem){
        Poglavje poglavje = new Poglavje();
        if(poglavjeItem != null){
            poglavje.setIdPoglavje(poglavjeItem.getIdPoglavje());
            poglavje.setIme(poglavjeItem.getIme());
            poglavje.setIdTecaj(poglavjeItem.getIdTecaj());
            poglavje.setIdNadrejenoPoglavje(poglavjeItem.getIdNadrejenoPoglavje());
            poglavje.setVsebina(NavodilaUtil.stringToByteArray(poglavjeItem.getVsebina()));
        }
        return poglavje;
    }

    public List<Poglavje> toPoglavjeList(List<PoglavjeItem> poglavjeItemList){
        return poglavjeItemList.stream()
                .map(this::toPoglavje)
                .collect(Collectors.toList());
    }
}
