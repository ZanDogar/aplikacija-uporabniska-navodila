package si.lj.uni.fri.dao;

import lombok.extern.log4j.Log4j;
import si.lj.uni.fri.database.entity.Poglavje;
import si.lj.uni.fri.exception.NavodilaException;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.List;
import java.util.StringJoiner;

@Log4j
@Stateless
public class PoglavjeDao extends BaseDao implements Serializable {

    private static final long serialVersionUID = -8894733634385932341L;

    public Poglavje getById(Long idPoglavje) throws NavodilaException {
        Poglavje result;
        try{
            StringJoiner sj = new StringJoiner(" ");
            sj.add("select p from poglavje p");
            sj.add("where p.idPoglavje = :idPoglavje");

            Query query = em.createQuery(sj.toString())
                    .setParameter("idPoglavje", idPoglavje);

            result = (Poglavje) query.getSingleResult();
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
        return result;
    }

    public List<Poglavje> getPoglavjaTecaja(Long idTecaj) throws NavodilaException {
        List<Poglavje> resultList = null;
        try {
            resultList = em.createQuery("select p from poglavje p where p.idTecaj = :idTecaj")
                    .setParameter("idTecaj", idTecaj)
                    .getResultList();
        } catch (NoResultException e){
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
        return resultList;
    }

    public void save(Poglavje poglavje){
        em.merge(poglavje);
    }

    public void dodajPoglavje(Poglavje poglavje) throws NavodilaException {
        try {
            em.persist(poglavje);
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
    }

    public void izbrisiPoglavja(List<Long> poglavjaIds) throws NavodilaException {
        try {
            em.createQuery("delete from poglavje p where p.idPoglavje in :poglavjaIds")
                    .setParameter("poglavjaIds", poglavjaIds)
                    .executeUpdate();
        } catch(PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
    }

    public void izbrisiPoglavjaTecaja(Long idTecaj) throws NavodilaException{
        try {
            em.createQuery("delete from poglavje p where p.idTecaj = :idTecaj")
                    .setParameter("idTecaj", idTecaj)
                    .executeUpdate();
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
    }
}
