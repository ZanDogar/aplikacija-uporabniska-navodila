package si.lj.uni.fri.dao;

import lombok.extern.log4j.Log4j;
import si.lj.uni.fri.database.entity.ClenZaporedja;
import si.lj.uni.fri.exception.NavodilaException;

import javax.ejb.Stateless;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

@Log4j
@Stateless
public class ClenZaporedjaDao extends BaseDao {

    public ClenZaporedja getById(Long idClenZaporedja) throws NavodilaException {
        ClenZaporedja result;
        try{
            Query query = em.createQuery("select c from clen_zaporedja c where c.idClenZaporedja = :idClenZaporedja")
                    .setParameter("idClenZaporedja", idClenZaporedja);
            result = (ClenZaporedja) query.getSingleResult();
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
        return result;
    }

    public void save(ClenZaporedja clenZaporedja){
        em.merge(clenZaporedja);
    }

    public ClenZaporedja dodajClenZaporedja(ClenZaporedja clenZaporedja) throws NavodilaException {
        try {
            em.persist(clenZaporedja);
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
        return clenZaporedja;
    }

    public void izbrisiCleneZaporedja(Long idZaporedje) throws NavodilaException {
        try {
            em.createQuery("delete from clen_zaporedja c where c.zaporedje.idZaporedje = :idZaporedje")
                    .setParameter("idZaporedje", idZaporedje)
                    .executeUpdate();
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
    }

    public void izbrisiNaslednjiClen(Long idClenZaporedja) throws NavodilaException {
        try {
            em.createQuery("update clen_zaporedja c set c.idNaslednjiClen.idClenZaporedja = :idNaslednjiClen where c.idClenZaporedja = :idClenZaporedja")
                    .setParameter("idNaslednjiClen", null)
                    .setParameter("idClenZaporedja", idClenZaporedja)
                    .executeUpdate();
        } catch (PersistenceException e){
            log.error(e);
            throw new NavodilaException(e);
        }
    }
}