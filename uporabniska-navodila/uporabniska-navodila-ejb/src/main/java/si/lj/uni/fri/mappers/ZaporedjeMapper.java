package si.lj.uni.fri.mappers;

import si.lj.uni.fri.database.entity.Zaporedje;
import si.lj.uni.fri.util.NavodilaUtil;
import si.lj.uni.fri.view.model.ZaporedjeItem;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.stream.Collectors;

@Named
public class ZaporedjeMapper {

    @Inject
    private ClenZaporedjaMapper clenZaporedjaMapper;

    public ZaporedjeItem toZaporedjeItem(Zaporedje zaporedje){
        ZaporedjeItem zaporedjeItem = new ZaporedjeItem();
        if(zaporedje != null){
            zaporedjeItem.setIdZaporedje(zaporedje.getIdZaporedje());
            zaporedjeItem.setIme(zaporedje.getIme());
            if(!NavodilaUtil.isListNullOrEmpty(zaporedje.getClenZaporedjaList())){
                zaporedjeItem.setClenZaporedjaItemList(clenZaporedjaMapper.toClenZaporedjaItemList(zaporedje.getClenZaporedjaList()));
            }
            zaporedjeItem.setIdPrviClenZaporedja(clenZaporedjaMapper.toClenZaporedjaItem(zaporedje.getIdPrviClenZaporedja(), true));
        }
        return zaporedjeItem;
    }

    public List<ZaporedjeItem> toZaporedjeItemList(List<Zaporedje> zaporedjeList){
        return zaporedjeList.stream()
                .map(this::toZaporedjeItem)
                .collect(Collectors.toList());
    }

    public Zaporedje toZaporedje(ZaporedjeItem zaporedjeItem){
        Zaporedje zaporedje = new Zaporedje();
        if(zaporedjeItem != null){
            zaporedje.setIdZaporedje(zaporedjeItem.getIdZaporedje());
            zaporedje.setIme(zaporedjeItem.getIme());
        }
        return zaporedje;
    }
}
