package si.lj.uni.fri.util;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

@Named
@RequestScoped
public class MessageUtil {

    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("i18n.messages");
    private static final String INFO_SUMMARY = "info";
    private static final String ERROR_SUMMARY = "error";

    public String getResourceMessage(String message) {
        String result = "";
        try{
            result = RESOURCE_BUNDLE.getString(message);
        } catch (MissingResourceException e){
            result = message;
        }
        return result;
    }

    public void showErrorMessage(String message){
        showMessage(FacesMessage.SEVERITY_ERROR, ERROR_SUMMARY, message);
    }

    public void showInfoMessage(String message){
        showMessage(FacesMessage.SEVERITY_INFO, INFO_SUMMARY, message);
    }

    public void showInfoMessage(String message, Object[] parameters){
        showMessage(FacesMessage.SEVERITY_INFO, INFO_SUMMARY, message, parameters);
    }

    private void showMessage(FacesMessage.Severity severity, String summary, String message){
        FacesContext.getCurrentInstance()
                .addMessage(null, new FacesMessage(severity, getResourceMessage(summary), getResourceMessage(message)));
    }

    private void showMessage(FacesMessage.Severity severity, String summary, String message, Object[] parameters){
        FacesContext.getCurrentInstance()
                .addMessage(null, new FacesMessage(severity, getResourceMessage(summary), MessageFormat.format(getResourceMessage(message), parameters)));
    }
}