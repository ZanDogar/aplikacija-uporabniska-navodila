package si.lj.uni.fri.database.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;


@Entity(name = "poglavje")
@Table(name = "poglavje")
@Data
public class Poglavje {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPoglavje;

    @Column
    private String ime;

    @Column
    private Long idTecaj;

    @Column
    private Long idNadrejenoPoglavje;

    @Column
    private byte[] vsebina;
}
