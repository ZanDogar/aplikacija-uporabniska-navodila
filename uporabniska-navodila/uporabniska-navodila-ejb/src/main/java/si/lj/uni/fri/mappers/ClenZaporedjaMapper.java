package si.lj.uni.fri.mappers;

import si.lj.uni.fri.database.entity.ClenZaporedja;
import si.lj.uni.fri.database.entity.Zaporedje;
import si.lj.uni.fri.util.NavodilaUtil;
import si.lj.uni.fri.view.model.ClenZaporedjaItem;
import si.lj.uni.fri.view.model.PoglavjeItem;

import javax.inject.Named;
import java.util.List;
import java.util.stream.Collectors;

@Named
public class ClenZaporedjaMapper {

    public ClenZaporedjaItem toClenZaporedjaItem(ClenZaporedja clenZaporedja, boolean nadaljuj){
        if(clenZaporedja != null){
            ClenZaporedjaItem clenZaporedjaItem = new ClenZaporedjaItem();

            clenZaporedjaItem.setIdClenZaporedja(clenZaporedja.getIdClenZaporedja());
            //clenZaporedjaItem.setZaporedjeItem(zaporedjeMapper.toZaporedjeItem(clenZaporedja.getZaporedje()));
            clenZaporedjaItem.setIme(clenZaporedja.getIme());
            clenZaporedjaItem.setVsebina(NavodilaUtil.byteArrayToString(clenZaporedja.getVsebina()));
            clenZaporedjaItem.setLongPrejsnjiClen(clenZaporedja.getIdPrejsnjiClen() != null ? clenZaporedja.getIdPrejsnjiClen().getIdClenZaporedja() : null);
            clenZaporedjaItem.setLongNaslednjiClen(clenZaporedja.getIdNaslednjiClen() != null ? clenZaporedja.getIdNaslednjiClen().getIdClenZaporedja() : null);
            clenZaporedjaItem.setIdNaslednjiClen(clenZaporedja.getIdNaslednjiClen() != null && nadaljuj ? toClenZaporedjaItem(clenZaporedja.getIdNaslednjiClen(), true) : null);
            clenZaporedjaItem.setIdPrejsnjiClen(clenZaporedja.getIdPrejsnjiClen() != null && nadaljuj ? toClenZaporedjaItem(clenZaporedja.getIdPrejsnjiClen(), false) : null);



            return clenZaporedjaItem;
        }
        return null;
    }

    public List<ClenZaporedjaItem> toClenZaporedjaItemList(List<ClenZaporedja> clenZaporedjaList) {
        return clenZaporedjaList.stream()
                .map(clenZaporedja -> toClenZaporedjaItem(clenZaporedja, true))
                .collect(Collectors.toList());
    }


    public ClenZaporedja toClenZaporedja(ClenZaporedjaItem clenZaporedjaItem, Zaporedje zaporedje) {
        if(clenZaporedjaItem != null){
            ClenZaporedja clenZaporedja = new ClenZaporedja();

            clenZaporedja.setIdClenZaporedja(clenZaporedjaItem.getIdClenZaporedja());
            clenZaporedja.setZaporedje(zaporedje);
            clenZaporedja.setIme(clenZaporedjaItem.getIme());
            clenZaporedja.setVsebina(NavodilaUtil.stringToByteArray(clenZaporedjaItem.getVsebina()));
            clenZaporedja.setIdPrejsnjiClen(clenZaporedjaItem.getIdPrejsnjiClen() != null ? toClenZaporedja(clenZaporedjaItem.getIdPrejsnjiClen(), zaporedje, false) : null);
            clenZaporedja.setIdNaslednjiClen(clenZaporedjaItem.getIdNaslednjiClen() != null ? toClenZaporedja(clenZaporedjaItem.getIdNaslednjiClen(), zaporedje, false) : null);
            return clenZaporedja;
        }
        return null;
    }

    public ClenZaporedja toClenZaporedja(ClenZaporedjaItem clenZaporedjaItem, Zaporedje zaporedje, boolean nadaljuj) {
        ClenZaporedja clenZaporedja = new ClenZaporedja();
        if(clenZaporedjaItem != null){
            clenZaporedja.setIdClenZaporedja(clenZaporedjaItem.getIdClenZaporedja());
            clenZaporedja.setZaporedje(zaporedje);
            clenZaporedja.setIme(clenZaporedjaItem.getIme());
            clenZaporedja.setVsebina(NavodilaUtil.stringToByteArray(clenZaporedjaItem.getVsebina()));
            clenZaporedja.setIdPrejsnjiClen(clenZaporedjaItem.getIdPrejsnjiClen() != null && nadaljuj ? toClenZaporedja(clenZaporedjaItem.getIdPrejsnjiClen(), zaporedje, false) : null);
            clenZaporedja.setIdNaslednjiClen(clenZaporedjaItem.getIdNaslednjiClen() != null && nadaljuj ? toClenZaporedja(clenZaporedjaItem.getIdNaslednjiClen(), zaporedje, false) : null);
        }
        return clenZaporedja;
    }

    public List<ClenZaporedja> toClenZaporedjaList(List<ClenZaporedjaItem> clenZaporedjaItemList, Zaporedje zaporedje){
        return clenZaporedjaItemList.stream().map(clenZaporedjaItem -> toClenZaporedja(clenZaporedjaItem, zaporedje, true)).collect(Collectors.toList());
    }

    public ClenZaporedja poglavjeItemToClenZaporedja(PoglavjeItem poglavjeItem, boolean nadaljuj){
        ClenZaporedja clenZaporedja = new ClenZaporedja();
        if(poglavjeItem != null){
            clenZaporedja.setIme(poglavjeItem.getIme());
            clenZaporedja.setVsebina(NavodilaUtil.stringToByteArray(poglavjeItem.getVsebina()));
            clenZaporedja.setIdPrejsnjiClen(poglavjeItem.getPrejsnjePoglavje() != null && nadaljuj ? poglavjeItemToClenZaporedja(poglavjeItem.getPrejsnjePoglavje(), false) : null);
            clenZaporedja.setIdNaslednjiClen(poglavjeItem.getNaslednjePoglavje() != null && nadaljuj ? poglavjeItemToClenZaporedja(poglavjeItem.getNaslednjePoglavje(), false) : null);
        }
        return clenZaporedja;
    }
}
