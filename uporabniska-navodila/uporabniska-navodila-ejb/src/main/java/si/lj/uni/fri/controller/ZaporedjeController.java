package si.lj.uni.fri.controller;

import si.lj.uni.fri.dao.ZaporedjeDao;
import si.lj.uni.fri.database.entity.ClenZaporedja;
import si.lj.uni.fri.database.entity.Zaporedje;
import si.lj.uni.fri.exception.NavodilaException;
import si.lj.uni.fri.mappers.ZaporedjeMapper;
import si.lj.uni.fri.util.NavodilaUtil;
import si.lj.uni.fri.view.model.ZaporedjeItem;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

@Named
public class ZaporedjeController implements Serializable {

    @Inject
    private ZaporedjeDao zaporedjeDao;

    @Inject
    private ZaporedjeMapper zaporedjeMapper;

    public List<ZaporedjeItem> getAll() throws NavodilaException {
        return zaporedjeMapper.toZaporedjeItemList(zaporedjeDao.getAll());
    }

    public ZaporedjeItem getById(Long idZaporedje) throws NavodilaException {
        return zaporedjeMapper.toZaporedjeItem(zaporedjeDao.getById(idZaporedje));
    }

    public void dodajZaporedje(ZaporedjeItem zaporedjeItem, List<ClenZaporedja> clenZaporedjaList) throws NavodilaException {
        Zaporedje zaporedje = zaporedjeMapper.toZaporedje(zaporedjeItem);

        ClenZaporedja root = null;
        if (!NavodilaUtil.isListNullOrEmpty(clenZaporedjaList)){
            ClenZaporedja tmpClenZaporedjaItem;
            ClenZaporedja prevClenZaporedjaItem = null;
            for (int i = 0; i < clenZaporedjaList.size(); i++) {
                if (i == 0) {
                    root = clenZaporedjaList.get(i);
                    tmpClenZaporedjaItem = root;
                } else {
                    tmpClenZaporedjaItem = clenZaporedjaList.get(i);
                }
                tmpClenZaporedjaItem.setZaporedje(zaporedje);

                if (prevClenZaporedjaItem != null) {
                    prevClenZaporedjaItem.setIdNaslednjiClen(clenZaporedjaList.get(i));
                    tmpClenZaporedjaItem.setIdPrejsnjiClen(prevClenZaporedjaItem);
                }
                prevClenZaporedjaItem = tmpClenZaporedjaItem;
            }
        } else {
            root = new ClenZaporedja();
            root.setZaporedje(zaporedje);
        }

        zaporedje.setClenZaporedjaList(Arrays.asList(root));
        zaporedjeDao.dodajZaporedje(zaporedje);
    }

    public void urediZaporedje(ZaporedjeItem zaporedjeItem) throws NavodilaException {
        zaporedjeDao.urediZaporedje(zaporedjeItem.getIdZaporedje(), zaporedjeItem.getIme());
    }

    public void izbrisiZaporedje(Long idZaporedje) throws NavodilaException {
        zaporedjeDao.izbrisiZaporedje(idZaporedje);
    }
}
