package si.lj.uni.fri.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Parameter {

    ID("id"),
    ID_POGLAVJE("idp"),
    ID_CLEN_ZAPOREDJA("idcz"),
    ERROR_MESSAGE("errorMessage"),
    MENI("meni"),
    TOPBAR("topbar");

    private String ime;

    public String ime() {
        return ime;
    }
}
