package si.lj.uni.fri.controller;

import lombok.extern.log4j.Log4j;
import si.lj.uni.fri.dao.UserDao;
import si.lj.uni.fri.database.entity.User;
import si.lj.uni.fri.exception.NavodilaException;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.DatatypeConverter;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Log4j
@Named
public class UserController implements Serializable {

    @Inject
    private UserDao userDao;

    public User getByUserLogin(String userName, String password) throws NavodilaException {
        return userDao.getByUserLogin(userName, getMd5Hash(password));
    }

    public void spremeniGeslo(Long idUser, String password) throws NavodilaException{
        userDao.spremeniGeslo(idUser, getMd5Hash(password));
    }

    private String getMd5Hash(String password) throws NavodilaException {
        String result = "";
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(password.getBytes());
            result = DatatypeConverter.printHexBinary(md5.digest());
        } catch (NoSuchAlgorithmException e) {
            log.error(e);
            throw new NavodilaException(e);
        }
        return result;
    }
}
