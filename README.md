1. Java install:
sudo apt-get install openjdk-8-jre


-----------------------------------------------------------------------------


2. WildFly install:
#1
sudo groupadd -r wildfly

#2
sudo useradd -r -g wildfly -d /opt/wildfly -s /sbin/nologin wildfly

#3
WILDFLY_VERSION=18.0.0.Final

#4
wget https://download.jboss.org/wildfly/$WILDFLY_VERSION/
wildfly-$WILDFLY_VERSION.tar.gz -P /tmp

#5
sudo tar xf /tmp/wildfly-$WILDFLY_VERSION.tar.gz -C /opt/

#6
sudo ln -s /opt/wildfly-$WILDFLY_VERSION /opt/wildfly

#7
sudo chown -RH wildfly: /opt/wildfly

#8
sudo mkdir -p /etc/wildfly

#9
sudo cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.conf 
/etc/wildfly/

#10
sudo cp /opt/wildfly/docs/contrib/scripts/systemd/launch.sh 
/opt/wildfly/bin/

#11
sudo sh -c 'chmod +x /opt/wildfly/bin/*.sh'

#12
sudo cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.service 
/etc/systemd/system/

#13
sudo systemctl daemon-reload

#14
sudo systemctl start wildfly

#15
sudo systemctl status wildfly

#16
sudo systemctl enable wildfly

#17
sudo ufw allow 8080/tcp


-----------------------------------------------------------------------------


3. Git install:
#1
sudo apt install git

#2
git clone https://ZanDogar@bitbucket.org/ZanDogar/aplikacija-uporabniska-navodila.git

#3
cd aplikacija-uporabniska-navodila/

#4
GIT_REPO=$(pwd)

#5
sudo cp $GIT_REPO/uporabniska-navodila/wildfly-datoteke/standalone.xml 
/opt/wildfly-$WILDFLY_VERSION/standalone/configuration/

#6
sudo cp $GIT_REPO/uporabniska-navodila/uporabniska-navodila-war/
target/uporabniska-navodila-war-1.0-SNAPSHOT.war 
/opt/wildfly-$WILDFLY_VERSION/standalone/deployments/

#7
sudo cp -r $GIT_REPO/uporabniska-navodila/wildfly-datoteke/mysql/ 
/opt/wildfly-$WILDFLY_VERSION/modules/system/layers/base/com/


-----------------------------------------------------------------------------


4. MySQL install:
#1
sudo apt install mysql-server

#2
sudo mysql

#3
CREATE USER 'sa'@'localhost' IDENTIFIED BY 'sa'

#4
GRANT ALL PRIVILEGES ON *.* TO 'sa'@'localhost' WITH GRANT OPTION;

#5
FLUSH PRIVILEGES;


-----------------------------------------------------------------------------


5. copy content of file /uporabniska-navodila/baza/init.sql to mySQL console